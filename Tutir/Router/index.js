import React from 'react'
import { StyleSheet, Text, View, Alert } from 'react-native'
import { NavigationContainer, DrawerActions  } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import firebase from 'firebase';

import Login from '../Screen/Login'
import Profile from '../Screen/Profile'
import Signup from '../Screen/Signup' 
import Home from '../Screen/Home' 
import Pengaduan from '../Screen/Pengaduan' 
import DataPengaduan from '../Screen/DataPengaduan' 
import About from '../Screen/About' 
 
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();




export default function Router() {
    return (
        <NavigationContainer>
			<Stack.Navigator screenOptions={{headerShown: false}} >
				<Stack.Screen name="Login" component={Login} />
				<Stack.Screen name="Home" component={Home} />
				<Stack.Screen name="Profile" component={Profile} /> 
				<Stack.Screen name="Signup" component={Signup} /> 
				<Stack.Screen name="Pengaduan" component={Pengaduan} /> 
				<Stack.Screen name="DataPengaduan" component={DataPengaduan} /> 
				<Stack.Screen name="About" component={About} /> 
				<Stack.Screen name="MainApp" component={MainApp} />
				<Stack.Screen name="MyDrawer" component={MyDrawer} />
			</Stack.Navigator>
		</NavigationContainer>
    )
} 


const MainApp = ()=>(
 
		   <Login />
	 
)

const doLogout = (props) => {

	 
	props.navigation.navigate("Login");
}




const konfirmasiLogout = (props) =>
	Alert.alert( 
		"Konfirmasi",
		"Yakin akan keluar dari aplikasi ?",
		[
		{
			text: "Cancel",
			onPress: () => console.log("Cancel Pressed"),
			style: "cancel"
		},
		{ text: "OK", onPress: () => doLogout(props)}
		],
		{ cancelable: false }
	);
 
const CustomDrawerContent = (props) => {
	 

  return (
	  
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label={() => <Text
	  	
		onPress={() => {konfirmasiLogout(props)}}
		>Logout</Text>}
        
        
      /> 
    </DrawerContentScrollView>

	
  );
};

const MyDrawer =()=>(
	<Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
	 	<Drawer.Screen name="Home" component={Home} /> 
		<Drawer.Screen name="Profile" component={Profile} /> 
		<Drawer.Screen name="Pengaduan" component={Pengaduan} /> 
		<Drawer.Screen name="Data Pengaduan" component={DataPengaduan} /> 
		<Drawer.Screen name="Tentang Aplikasi" component={About} /> 
      
	</Drawer.Navigator>
)

const styles = StyleSheet.create({})
