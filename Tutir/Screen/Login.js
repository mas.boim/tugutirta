
import React, {useEffect, Component, useState} from 'react'
import { View, Dimensions, PixelRatio, Button, Platform, Text, Alert, ActivityIndicator,
		Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput } from 'react-native'
	import firebase from 'firebase';	
 


var winWidth = Dimensions.get('window').width; 
const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');


export default function Login({navigation}) {
		
		
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState(""); 
	const [loading, setLoading] = useState(false); 
	
	
	

	const submit = () => {
		   
		if (!email) {
			alert("Masukkan Email")			
			return;
		}

		if (!password) {
			alert('Masukkan password');
			return;
		}

		setLoading(true);

			firebase.auth().signInWithEmailAndPassword(email, password)
			.then(() => {
					setLoading(false);

					navigation.navigate('MyDrawer', {
						email: email,
						screen: 'App'
					})

					setPassword('')
					setEmail('')

					
			})
			.catch((error) => {
					setLoading(false);
					console.log(error)
				 	alert('Data Pengguna tidak ditemukan !! ' + error.message);
			})



		};


		
    const renderButtonOrLoading = () => {
        if (loading) {
            return  <View style={[styles.boxButton]}>									 
											<ActivityIndicator size="large" color="#00ff00" />
										</View>
        }

				return 	(
					<View  style={styles.boxButton}>
					<TouchableOpacity
							 style={styles.buttonLogin}
							 onPress={submit}
						   >
						<Text  style={styles.buttonLoginCaption}> LOGIN </Text>
					</TouchableOpacity>
					<TouchableOpacity
							 style={styles.buttonDaftar}
							 onPress={() => navigation.navigate("Signup")}
						   >
						<Text  style={styles.buttonDaftarCaption}> DAFTAR </Text>
					</TouchableOpacity>
					</View>
				)
    };



    return (
       <View style={styles.container}>
				<View style={[styles.boxHeader]}>
					<Image				
						source={require('./asset/logo.png')}
						style={[styles.logoHeader]}
						 
					/>
				</View>
				<View style={[styles.boxContent]}>
					<View style={styles.captionLoginView}>
					  <Text style={styles.captionLogin}>Masukkan Email dan Kata Sandi Untuk Masuk Ke Sistem</Text>
					</View>
					<View style={styles.inputView}>
					  <TextInput
						style={styles.TextInput}
						placeholder="Email"
						placeholderTextColor="#003f5c" 
						 value={email}
						onChangeText={(value)=>setEmail(value)}
					  />
					</View>
					 
					<View style={styles.inputView}>
					  <TextInput
						style={styles.TextInput}
						placeholder="Password"
						placeholderTextColor="#003f5c"
						secureTextEntry={true} 
						  value={password}
							onChangeText={(value)=>setPassword(value)}
					  />
					</View>
					{renderButtonOrLoading()}
				</View> 
			</View>
    )
}
 
 
 

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor: '#FFF',
		paddingTop: 150
	},
	boxHeader: {
		flex: 0.2,  
	},
	logoHeader : {
		flex: 1, 
		alignSelf: 'center',		
		width: winWidth * 0.7,	 
		resizeMode: 'contain'
		
	},
	boxContent: {
		flex: 0.8
	},
	captionLoginView: {		
		marginBottom: 10,
		marginTop: 20,
		width: "70%",
		alignSelf: 'center',
		alignItems: "center",
	},
	captionLogin: {		
		fontWeight: 'bold',  
		alignSelf: 'center',
		textAlign: 'center'
	},
	inputView: {
	   backgroundColor: "#FFF",
	   borderColor: "#000",
	   borderWidth: 1,
	   borderRadius: 10,
	   width: "70%",
	   height: 45,
	   marginBottom: 20,
	   alignItems: "center",
	   alignSelf: 'center',
	 },
	TextInput: {
	   height: 50,
	   flex: 1,
		 paddingLeft: 10,
		 paddingRight: 10,
	   marginLeft: 0,
	   alignSelf: 'center',
		 alignItems: "center",
		width: "100%"
	},
	buttonView: {
		alignSelf: 'center',
		alignItems: "center", 
		
		 
	},
	buttonLogin: { 
		width: "70%",
		backgroundColor: "#1E3163", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonLoginCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	buttonDaftar: { 
		width: "70%",
		backgroundColor: "#a0d258", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		marginTop: 10, 
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonDaftarCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	boxButton : {
		flex: 1
	}	
	
})
