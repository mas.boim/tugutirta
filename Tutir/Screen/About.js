
import React, {useEffect, Component, useState, useFocusEffect} from 'react'
import { View, Dimensions, PixelRatio, Button, Platform, Text, 
		Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput } from 'react-native'
import { useIsFocused } from "@react-navigation/native";
import firebase from 'firebase';	
import _ from "lodash";

var winWidth = Dimensions.get('window').width; 
const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}   
 
export default function About({navigation}) {
	const [namalengkap, setNamaLengkap] = useState("");
	const isFocused = useIsFocused();
	

	useEffect(() => {
	
		setNamaLengkap('Loading...');

		firebase
		.firestore()
		.collection('sipdam')
		.doc(firebase.auth().currentUser.uid).get()
		.then((doc) => {
			if (doc.exists) {				
				const user = doc.data(); 
				setNamaLengkap(user.namalengkap)
				
			} else {
				// doc.data() will be undefined in this case
				console.log("No such document!");
			}

			setLoading(false);
		}).catch((error) => {
			console.log("Error getting document:", error);
		});
	  }, [navigation, isFocused]);
	 
		
	return (
		<View style={styles.container}>
			<View style={[styles.boxHeader]}>
				<Image				
					source={require('./asset/logo.png')}
					style={[styles.logoHeader]}
						
				/>
			</View>
			
			<View style={[styles.boxContent]}>
				 				 
				<View style={styles.boxKantor}>
					<Image				
					source={require('./asset/profile.jpg')}
					style={[styles.fotoKantor]}						
					/>  		
					<Text style={{width: winWidth * 0.7, alignSelf: 'center', alignItems: 'center'}}>
						Aplikasi ini dikembangkan sebagai tugas akhir sanbercode {'\n'}{'\n'}
						Oleh : Agung Putra Kusuma{'\n'}{'\n'}
						Email : mas.boim@gmail.com {'\n'}{'\n'}
						WA : mas.boim@gmail.com {'\n'}{'\n'}{'\n'}
						Terima Kasih
						
					</Text>			
				</View> 	
			</View> 
		</View>
		
	);
  
}

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor: '#FFF'
	},
	boxHeader: {
		flex: 0.2,  
		marginTop: 10
	},
	logoHeader : {
		flex: 1, 
		alignSelf: 'center',		
		width: winWidth * 0.7,	 
		resizeMode: 'contain'
		
	},
	boxKantor: {
		marginTop: 20,
		alignItems: 'center',	
		alignSelf: 'center'
	},
	fotoKantor : {
	 
		alignSelf: 'center',		
		width: winWidth * 0.7,	 
		resizeMode: 'contain', 
		height: 200,
		marginBottom: 20
		
	},
	boxContent: {
		flex: 0.8
	},
	profileHeader: {	 
		backgroundColor: "#FFF",
		justifyContent: "center",
		flex: 0.2,  
		flexDirection: "row"
	},
	 
	avatar: {		 			 
		width: 100,
		height: 100,		 
		borderRadius: 50,		
		resizeMode: 'contain'
	},
	profileDetail: {
		flex: 1,  
		width: "100%", 
		alignSelf: 'center',
		alignItems: 'center',
		marginLeft: 10,
	},
	profileName: {
		fontSize: 14,
		fontWeight: "bold", 
		alignSelf: 'center',
		marginTop: 5,
		
	},
	profileEmail: {
		 
		fontSize: 12,
		marginTop: 5,
	},
	profileJob: {
		fontSize: 12,
		marginTop: 5,
		fontWeight: "bold",
	},
	profileSkill: {
		width: "80%",
		alignSelf: 'center',
		backgroundColor: "#FFF",
	},
	profileSkillIcon: {
		width: 20,
		height: 20
	},
	profileSkillHeader: {
		flexDirection: "row", 
	},
	profileSkillCaption: {
		marginLeft: 10,
		fontSize: 12,
		fontWeight: "bold",
	},	
	profileDetailSkill: {
		flex: 1,
		marginTop: 30,
		width: "100%"
	},
	profileSkillDetail: {
		flexDirection: "row",
		alignSelf: 'center',			
		width: "100%",
		height: "20%",
		marginBottom: 10,
		backgroundColor: "#FFF",
	},
	profileSkillDetailA: {
		flex: 0.6,  
		marginLeft: 10,
		backgroundColor: "#FFF",
		flexDirection: "row",
	},
	profileSkillDetailB: {
		flex: 0.4,  
		marginTop: 3,
		marginRight: 10,
		backgroundColor: "#FFF",
		flexDirection: "row",
		justifyContent: 'flex-end'
	},
	profileSkillDetailIcon: {
		width: 20,
		height: 20
	},
	profileSkillDetailCaption: {
		marginLeft: 7,
		fontSize: 12, 
	},
	profileSkillLevelIcon: {
		width: 15,
		height: 15,
		marginRight: 5,
	},
	
	
	
	
})
