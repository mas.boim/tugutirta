
import React, {useEffect, Component, useState} from 'react'
import { View, Dimensions, PixelRatio, Button, Platform, Text, ActivityIndicator, Alert,
		Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput } from 'react-native'
import firebase from 'firebase';	
import { useIsFocused } from "@react-navigation/native";
import _ from "lodash";


var winWidth = Dimensions.get('window').width; 

export function generateUUID(digits) {
    let str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXZ';
    let uuid = [];
    for (let i = 0; i < digits; i++) {
        uuid.push(str[Math.floor(Math.random() * str.length)]);
    }
    return uuid.join('');
}

export default function Pengaduan({navigation}) {
	 
	const [namalengkap, setNamaLengkap] = useState("");  
	const [nosal, setNosal] = useState(""); 
	const [pengaduan, setPengaduan] = useState(""); 
	
	const [loading, setLoading] = useState(false); 

	const isFocused = useIsFocused();
	
	useEffect(() => {
    // Good!
		firebase
		.firestore()
		.collection('sipdam')
		.doc(firebase.auth().currentUser.uid).get()
		.then((doc) => {
			if (doc.exists) {
				 
			

				const user = doc.data(); 
				setNosal(user.nosal)
				setNamaLengkap(user.namalengkap)
				
			} else {
				// doc.data() will be undefined in this case
				console.log("No such document!");
			}

			setLoading(false);
		}).catch((error) => {
			alert(error)
			setLoading(false);
		});
 	 }, [navigation, isFocused]);
	

	
	
	
	const konfirmasiPengaduan = () =>
	Alert.alert( 
		"Konfirmasi",
		"Yakin akan melakukan pengaduan ?",
		[
		{
			text: "Cancel",
			onPress: () => console.log("Cancel Pressed"),
			style: "cancel"
		},
		{ 
			text: "OK",
			onPress: () => {
				setLoading(true);
			 	var date = new Date().getDate(); //Current Date
				var month = new Date().getMonth() + 1; //Current Month
				var year = new Date().getFullYear(); //Current Year
				var hours = new Date().getHours(); //Current Hours
				var min = new Date().getMinutes(); //Current Minutes
				var sec = new Date().getSeconds(); //Current Seconds
				var tanggalPengaduan =  date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec
				
				firebase
				.firestore()
				.collection('pengaduan')
				.doc(generateUUID(20))
				.set({
						iduser: firebase.auth().currentUser.uid,						
						namalengkap: namalengkap,
						keterangan: pengaduan,
						nosal: nosal,
						nomor: generateUUID(5),
						tanggal: tanggalPengaduan
				})
				.then(() => {
					
					navigation.navigate("Data Pengaduan")
					alert('Data Pengaduan Sukses ditambahkan ');
					
					setNamaLengkap('')
					setNosal('')
					setPengaduan('')

				})
				.catch((error) => {
					alert(error);
				}); 
								
					 
			}
		
		
		}
	],
		{ cancelable: false }
	);

	

	const cekValidasiPengaduan = () => {
			   
			if (!namalengkap) {
				alert("Masukkan Nama Lengkap")			
				return;
			}

			if (!nosal) {
				alert("Masukkan Nomor Pelanggan")			
				return;
			}

			

			if (!pengaduan) {
				alert("Masukkan Pengaduan")			
				return;
			}
			

			


			konfirmasiPengaduan()
		};

 
	
		
    const renderButtonOrLoading = () => {
        if (loading) {
            return  <View style={[styles.boxButton]}>									 
						<ActivityIndicator size="large" color="#00ff00" />
					</View>
        }

		return 	(
			<View  style={styles.boxButton}>
			<TouchableOpacity
						style={styles.buttonLogin}
						onPress={cekValidasiPengaduan}
					>
				<Text  style={styles.buttonLoginCaption}> SIMPAN </Text>
			</TouchableOpacity>			 
			</View>
		)
    };



 
	return (
		<View style={styles.container}>
			<View style={[styles.boxHeader]}>
				<Image				
					source={require('./asset/logo.png')}
					style={[styles.logoHeader]}
						
				/>
			</View>
			<View style={[styles.boxContent]}>
				<View style={styles.captionLoginView}>
					<Text style={styles.captionLogin}>PENGADUAN AIR </Text>
				</View>
				 
				<View style={styles.inputView}>
					<TextInput
					style={styles.TextInput}
					placeholder="Nama Lengkap"
					placeholderTextColor="#003f5c" 
						value={namalengkap}
					onChangeText={(value)=>setNamaLengkap(value)}
					/>
				</View>
				<View style={styles.inputView}>
					<TextInput
					style={styles.TextInput}
					placeholder="Nomor Pelanggan"
					placeholderTextColor="#003f5c" 
					value={nosal}
					keyboardType={'numeric'}
					onChangeText={(value)=>setNosal(value)}
					/>
				</View>
				<View style={styles.inputViewMultiline}>
					<TextInput
					style={styles.TextInputMultiline}
					placeholder="Pengaduan"
					placeholderTextColor="#003f5c" 
						value={pengaduan}
					multiline={true}
					onChangeText={(value)=>setPengaduan(value)}
					/>
				</View>
				
					 
				{renderButtonOrLoading()}
			</View> 
		</View>
		
	);
  
}

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor: '#FFF',
		paddingTop: 100
	},
	boxHeader: {
		flex: 0.2,  
	},
	logoHeader : {
		flex: 1, 
		alignSelf: 'center',		
		width: winWidth * 0.7,	 
		resizeMode: 'contain'
		
	},
	boxContent: {
		flex: 0.8
	},
	captionLoginView: {		
		marginBottom: 10,
		marginTop: 20,
		width: "70%",
		alignSelf: 'center',
		alignItems: "center",
	},
	captionLogin: {		
		fontWeight: 'bold',  
		alignSelf: 'center',
		textAlign: 'center'
	},
	inputView: {
	   backgroundColor: "#FFF",
	   borderColor: "#000",
	   borderWidth: 1,
	   borderRadius: 10,
	   width: "70%",
	   height: 45,
	   marginBottom: 20,
	   alignItems: "center",
	   alignSelf: 'center',
	 },
	TextInput: {
	   height: 50,
	   flex: 1,
		 paddingLeft: 10,
		 paddingRight: 10,
	   marginLeft: 0,
	   alignSelf: 'center',
		 alignItems: "center",
		width: "100%"
	},
	inputViewMultiline: {
	   backgroundColor: "#FFF",
	   borderColor: "#000",
	   borderWidth: 1,
	   borderRadius: 10,
	   width: "70%",
	   height: 90,
	   marginBottom: 20,
	   alignItems: "center",
	   alignSelf: 'center',
	 },
	TextInputMultiline: {
		height: 80,
		flex: 1,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 10,
		marginLeft: 0,
		alignSelf: 'center',
		alignItems: "center",
		width: "100%",
		textAlignVertical: 'top'
	},

	
	buttonView: {
		alignSelf: 'center',
		alignItems: "center", 
		
		 
	},
	buttonLogin: { 
		width: "70%",
		backgroundColor: "#1E3163", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonLoginCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	buttonDaftar: { 
		width: "70%",
		backgroundColor: "#a0d258", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		marginTop: 10, 
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonDaftarCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	boxButton : {
		flex: 1
	}	
	
})

