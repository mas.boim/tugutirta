
import React, {useEffect, Component, useState} from 'react'
import { View, Dimensions, PixelRatio, Button, Platform, Text, ActivityIndicator, Alert, LogBox,
		Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput } from 'react-native'
import { useIsFocused } from "@react-navigation/native";
import firebase from 'firebase';	
LogBox.ignoreLogs(['Setting a timer']);
	 
var winWidth = Dimensions.get('window').width; 
const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');
 
// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}  

export default function Signup({navigation}) {


	const [email, setEmail] = useState("");
	const [namalengkap, setNamaLengkap] = useState("");	
	const [password, setPassword] = useState(""); 
	const [konfirmasi_password, setKonfirmasiPassword] = useState(""); 
	const [loading, setLoading] = useState(false); 

	  const isFocused = useIsFocused();
    
   

    useEffect(() => {
         	setNamaLengkap('')
					setEmail('') 
					setKonfirmasiPassword('')

	  }, [navigation, isFocused]);
		
	
	const konfirmasiDaftar = () =>
		Alert.alert( 
			"Konfirmasi",
			"Yakin akan daftar ke aplikasi ?",
			[
			{
				text: "Cancel",
				onPress: () => console.log("Cancel Pressed"),
				style: "cancel"
			},
			{ text: "OK", onPress: () => {
					setLoading(true);
					firebase
					.auth()
					.createUserWithEmailAndPassword(email, password)
					.then((res) => {
						
						res.user.updateProfile({
							displayName: namalengkap
						})
 
						 firebase
						.firestore()
						.collection('sipdam')
						.doc( res.user.uid)
						.set({
								email: email,
								namalengkap: namalengkap,
								alamat: '',
								nosal: ''
						})
						.then(() => {
							
								alert('Data Pengguna Sukses Didaftarkan, Silahkan Login Menggunakan Email yang sudah terdaftar !! ');
								navigation.navigate("Login")
						})
						.catch((error) => {
								alert(error);
						}); 
								 
						
						
					})
					.catch(error => 
						{
								alert(error.message);
								setLoading(false)
						})   
			}
			
			
			 }
			],
			{ cancelable: false }
		);
	


	const prosesPendaftaran = () => {
		   
				if (!email) {
					alert("Masukkan Email")			
					return;
				}

				if (!namalengkap) {
					alert("Masukkan Nama Lengkap")			
					return;
				}

				if (!password) {
					alert('Masukkan password');
					return;
				}

				if (password.length < 6){
					alert('Password minimal 6 karakter');
					return;
				}

				if (password != konfirmasi_password) {
					alert('Password tidak sama dengan konfirmasi');
					return;
				}
 

				konfirmasiDaftar()
		};


		
		
    const renderButtonOrLoading = () => {
        if (loading) {
            return  <View style={[styles.boxButton]}>									 
											<ActivityIndicator size="large" color="#00ff00" />
										</View>
        }

				return 	(
					<View  style={styles.boxButton}>				 
					<TouchableOpacity
							 style={styles.buttonLogin}
							  onPress={prosesPendaftaran}
						   >
						<Text  style={styles.buttonLoginCaption}> DAFTAR </Text>
					</TouchableOpacity>
					<TouchableOpacity
							 style={styles.buttonDaftar}
							 onPress={() => navigation.navigate("Login")}
						   >
						<Text  style={styles.buttonDaftarCaption}> KEMBALI  </Text>
					</TouchableOpacity>

					</View>
				)
    };




		return (
			<View style={styles.container}>
				<View style={[styles.boxHeader]}>
					<Image				
						source={require('./asset/logo.png')}
						style={[styles.logoHeader]}
						 
					/>
				</View>
				<View style={[styles.boxContent]}>
					<View style={styles.captionLoginView}>
					  <Text style={styles.captionLogin}>Masukkan Data Lengkap Untuk Melakukan Pendaftaran</Text>
					</View>
					<View style={styles.inputView}>
					  <TextInput
						style={styles.TextInput}
						placeholder="Email"
						value={email}
						placeholderTextColor="#003f5c" 
						onChangeText={(value)=>setEmail(value)}
					  />
					</View>		

					<View style={styles.inputView}>
					  <TextInput
						style={styles.TextInput}
						placeholder="Nama Lengkap"
						value={namalengkap}
						placeholderTextColor="#003f5c" 
						onChangeText={(value)=>setNamaLengkap(value)}
					  />
					</View>				 
					<View style={styles.inputView}>
					  <TextInput
						style={styles.TextInput}
						placeholder="Kata Sandi"
						value={password}
						placeholderTextColor="#003f5c"
						secureTextEntry={true} 
						onChangeText={(value)=>setPassword(value)}
					  />
					</View>
					 
					<Text  style={styles.textHintKarakter}> Minimal 6 Karakter</Text>
				 
					<View style={styles.inputView}>
					  <TextInput
						style={styles.TextInput}
							value={konfirmasi_password}
						placeholder="Konfirmasi Kata Sandi"
						placeholderTextColor="#003f5c"
						onChangeText={(value)=>setKonfirmasiPassword(value)}
						secureTextEntry={true} 
					  />
					</View> 
					{renderButtonOrLoading()}
					
				</View> 
			</View>
			
		);
  }
 

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor: '#FFF'
	},
	boxHeader: {
		flex: 0.2,  
		marginTop: 10
	},
	logoHeader : {
		flex: 1, 
		alignSelf: 'center',		
		width: winWidth * 0.7,	 
		resizeMode: 'contain'
		
	},
	boxContent: {
		flex: 0.8
	},
	captionLoginView: {		
		marginBottom: 10,
		marginTop: 20,
		width: "70%",
		alignSelf: 'center',
		alignItems: "center",
	},
	captionLogin: {		
		fontWeight: 'bold',  
		alignSelf: 'center',
		textAlign: 'center'
	},
	inputView: {
	   backgroundColor: "#FFF",
	   borderColor: "#000",
	   borderWidth: 1,
	   borderRadius: 10,
	   width: "70%",
	   height: 45,
	   marginTop: 20,
	   alignItems: "center",
	   alignSelf: 'center',
	 },
	TextInput: {
	   height: 50,
	   flex: 1,
	   paddingLeft: 10,
		 paddingRight: 10,
	   width: "100%",
	   marginLeft: 0,
	   alignSelf: 'center',
		 alignItems: "center",
				
	},
	buttonView: {
		alignSelf: 'center',
		alignItems: "center", 
		
		 
	},  
	buttonLogin: { 
		marginTop: 10,
		width: "70%",
		backgroundColor: "#1E3163", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonLoginCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	buttonDaftar: { 
		width: "70%",
		backgroundColor: "#a0d258", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		marginTop: 10, 
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonDaftarCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	textHintKarakter: {
		fontWeight: 'bold',  
		alignSelf: 'center',
		textAlign: 'center',
		marginTop: 5,
		fontSize: 10,
	},
	boxButton : {
		flex: 1,
		marginTop: 10
	}	
	
	
	
})
