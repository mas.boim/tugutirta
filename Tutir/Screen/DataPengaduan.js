import React, { useEffect } from 'react'
import { useState } from 'react'
import { ActivityIndicator, StyleSheet, Text, View, Image, Button, FlatList, TouchableOpacity } from 'react-native'
import firebase from 'firebase';
import { useIsFocused } from "@react-navigation/native";

export default function DataPengaduan({route, navigation}) {
    
    const [items, setItems] = useState([]); 
    const [loading, setLoading] = useState(false); 
    const isFocused = useIsFocused();
    
   

    useEffect(() => {
         setLoading(false);
        firebase
		.firestore()
        .collection('pengaduan')
        .orderBy("tanggal", "desc")
         .get()
       .then((querySnapshot) => {
            const dataPengaduan = [];
            
             querySnapshot.forEach(documentSnapshot => {     
                console.log(documentSnapshot.data())       
                if (firebase.auth().currentUser.uid == documentSnapshot.data().iduser) {
                    dataPengaduan.push(documentSnapshot.data())
                }    
                
            });
            setItems(dataPengaduan) 
        })
        .catch((error) => {
            console.log("Error getting documents: ", error);
            alert(error);
        }); 

	  }, [navigation, isFocused]);

 
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"center", alignItems: 'center', padding: 16}}>
                <Text>Berikut Adalah Data Pengaduan Anda </Text>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60, flex: 1}}>
			<FlatList
				style={{flex: 1, width: "100%"}}
				data={items}
				keyExtractor={(item) => `${item.nomor}`}
				renderItem={({item}) => {
					return (
					 
							<View style={{flex: 1, width: "100%", paddingLeft: 10, paddingRight: 10, paddingBottom: 5, paddingTop: 5, 
									borderBottomWidth: 1, borderBottomColor: '#ddd'}}>
                                <View style={{ flexDirection: 'row'}}>
                                    <Text style={{width: 120}}>No. Pengaduan</Text>    
                                    <Text style={{fontWeight: 'bold', color: '#000', flex: 1, width: '100%'}}>: SPK-{(item.nomor).toUpperCase()}</Text>
                                </View>
								<View style={{ flexDirection: 'row'}}>
                                    <Text style={{width: 120}}>Tanggal</Text>    
                                    <Text>: {item.tanggal}</Text>
                                </View>
								<View style={{ flexDirection: 'row'}}>
                                    <Text style={{width: 120}}>No. Pelanggan</Text>    
                                    <Text>: {item.nosal}</Text>
                                </View>
                                <View style={{ flexDirection: 'row'}}>
                                    <Text style={{width: 120}}>Nama</Text>    
                                    <Text>: {item.namalengkap}</Text>
                                </View>
								<View style={{ flexDirection: 'row'}}>
                                    <Text style={{width: 120}}>Pengaduan</Text>    
                                    <Text>: {item.keterangan}</Text>
                                </View>
								
							</View>
							
							 
					)
				}}
			/>
				 
            
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
	itemList: {
		flex: 1,
		
	},
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
        
})
