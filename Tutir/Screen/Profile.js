
import React, {useEffect, Component, useState} from 'react'
import { View, Dimensions, PixelRatio, Button, Platform, Text, ActivityIndicator, Alert,
		Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, TextInput } from 'react-native'
import firebase from 'firebase';	
import _ from "lodash";

var winWidth = Dimensions.get('window').width; 

export default function Profile({navigation}) {
	
		
	const [email, setEmail] = useState("");
	const [namalengkap, setNamaLengkap] = useState(""); 
	const [alamat, setAlamat] = useState(""); 
	const [nosal, setNosal] = useState(""); 
	const [loading, setLoading] = useState(false); 
	
	useEffect(() => {
    // Good!
		firebase
		.firestore()
		.collection('sipdam')
		.doc(firebase.auth().currentUser.uid).get()
		.then((doc) => {
			if (doc.exists) {
				  
				const user = doc.data();
				setAlamat(user.alamat); 
				setNosal(user.nosal)
				setNamaLengkap(user.namalengkap)
				
			} else {
				// doc.data() will be undefined in this case
				console.log("No such document!");
			}

			setLoading(false);
		}).catch((error) => {
			console.log("Error getting document:", error);
		});
 	 }, []);
	
	
	
	const konfirmasiUpdateProfile = () =>
	Alert.alert( 
		"Konfirmasi",
		"Yakin akan update profile ?",
		[
		{
			text: "Cancel",
			onPress: () => console.log("Cancel Pressed"),
			style: "cancel"
		},
		{ 
			text: "OK",
			onPress: () => {
				setLoading(true);
			 	
				firebase
				.firestore()
				.collection('sipdam')
				.doc(firebase.auth().currentUser.uid)
				.update({
						email: email,
						namalengkap: namalengkap,
						alamat: alamat,
						nosal: nosal
				})
				.then(() => {
					const currentUser = firebase.auth().currentUser;
					currentUser.updateProfile({
						displayName: namalengkap
					}).then(() => {
						setLoading(false);
						navigation.navigate("Home")
						alert('Data Pengguna Sukses diupdate ');


					}).catch((error) => {
						
					});  
					
					
				})
				.catch((error) => {
					alert(error);
				}); 
								
					 
			}
		
		
		}
	],
		{ cancelable: false }
	);
 
	
		
    const renderButtonOrLoading = () => {
        if (loading) {
            return  <View style={[styles.boxButton]}>									 
						<ActivityIndicator size="large" color="#00ff00" />
					</View>
        }

		return 	(
			<View  style={styles.boxButton}>
			<TouchableOpacity
						style={styles.buttonLogin}
						onPress={konfirmasiUpdateProfile}
					>
				<Text  style={styles.buttonLoginCaption}> SIMPAN </Text>
			</TouchableOpacity>			 
			</View>
		)
    };



 
	return (
		<View style={styles.container}>
			<View style={[styles.boxHeader]}>
				<Image				
					source={require('./asset/logo.png')}
					style={[styles.logoHeader]}
						
				/>
			</View>
			<View style={[styles.boxContent]}>
				<View style={styles.captionLoginView}>
					<Text style={styles.captionLogin}>Ubah Profile </Text>
				</View>
				<View style={styles.inputView}>
					<TextInput
					style={styles.TextInput}
					placeholder="Email"
					readonly="true"
					placeholderTextColor="#003f5c" 
					editable = {false}
					value={firebase.auth().currentUser.email} 
					/>
				</View>
				<View style={styles.inputView}>
					<TextInput
					style={styles.TextInput}
					placeholder="Nama Lengkap"
					placeholderTextColor="#003f5c" 
						value={namalengkap}
					onChangeText={(value)=>setNamaLengkap(value)}
					/>
				</View>
				<View style={styles.inputView}>
					<TextInput
					style={styles.TextInput}
					placeholder="Nomor Pelanggan"
					placeholderTextColor="#003f5c" 
					value={nosal}
					keyboardType={'numeric'}
					onChangeText={(value)=>setNosal(value)}
					/>
				</View>
				<View style={styles.inputViewMultiline}>
					<TextInput
					style={styles.TextInputMultiline}
					placeholder="Alamat"
					placeholderTextColor="#003f5c" 
						value={alamat}
					multiline={true}
					onChangeText={(value)=>setAlamat(value)}
					/>
				</View>
				
					 
				{renderButtonOrLoading()}
			</View> 
		</View>
		
	);
  
}

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor: '#FFF',
		paddingTop: 100
	},
	boxHeader: {
		flex: 0.2,  
	},
	logoHeader : {
		flex: 1, 
		alignSelf: 'center',		
		width: winWidth * 0.7,	 
		resizeMode: 'contain'
		
	},
	boxContent: {
		flex: 0.8
	},
	captionLoginView: {		
		marginBottom: 10,
		marginTop: 20,
		width: "70%",
		alignSelf: 'center',
		alignItems: "center",
	},
	captionLogin: {		
		fontWeight: 'bold',  
		alignSelf: 'center',
		textAlign: 'center'
	},
	inputView: {
	   backgroundColor: "#FFF",
	   borderColor: "#000",
	   borderWidth: 1,
	   borderRadius: 10,
	   width: "70%",
	   height: 45,
	   marginBottom: 20,
	   alignItems: "center",
	   alignSelf: 'center',
	 },
	TextInput: {
	   height: 50,
	   flex: 1,
		 paddingLeft: 10,
		 paddingRight: 10,
	   marginLeft: 0,
	   alignSelf: 'center',
		 alignItems: "center",
		width: "100%"
	},
	inputViewMultiline: {
	   backgroundColor: "#FFF",
	   borderColor: "#000",
	   borderWidth: 1,
	   borderRadius: 10,
	   width: "70%",
	   height: 90,
	   marginBottom: 20,
	   alignItems: "center",
	   alignSelf: 'center',
	 },
	TextInputMultiline: {
		height: 80,
		flex: 1,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 10,
		marginLeft: 0,
		alignSelf: 'center',
		alignItems: "center",
		width: "100%",
		textAlignVertical: 'top'
	},

	
	buttonView: {
		alignSelf: 'center',
		alignItems: "center", 
		
		 
	},
	buttonLogin: { 
		width: "70%",
		backgroundColor: "#1E3163", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonLoginCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	buttonDaftar: { 
		width: "70%",
		backgroundColor: "#a0d258", 
		height: 50, 
		alignSelf: 'center',
		alignItems: "center", 
		borderRadius: 10,
		marginTop: 10, 
		textAlignVertical: 'center',		
		flexDirection: 'row', 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	buttonDaftarCaption: {
		color: '#FFF',
		fontWeight: 'bold',
		fontSize: 13,
		alignItems: "center", 
		
	},
	boxButton : {
		flex: 1
	}	
	
})

