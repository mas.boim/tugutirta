import { StatusBar } from 'expo-status-bar';
import React from 'react';
import 'react-native-gesture-handler';
import { StyleSheet, Text, View } from 'react-native';
import Tutir from './Tutir';


import firebase from 'firebase';
 

// Dibawah Ini Merupakan inisialisasi Firebase dari Website
firebase.initializeApp({
 apiKey: "AIzaSyAA79FfdtAuOnPf-Eip9MxKlKNFPVWI6n0",
  authDomain: "tugu-tirta.firebaseapp.com",
  databaseURL: "https://tugu-tirta-default-rtdb.firebaseio.com",
  projectId: "tugu-tirta",
  storageBucket: "tugu-tirta.appspot.com",
  messagingSenderId: "617959329255",
  appId: "1:617959329255:web:ad4dffdc68fa520f5f8142",
  measurementId: "G-23FM3HJV6G"
});

export default function App() {
  return (
    <Tutir/>    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
